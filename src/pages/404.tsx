import { useEffect } from 'react';
import { useRouter } from 'next/router';
import * as routes from '@/common/constants/routes';

const NotFoundPage = () => {
  const router = useRouter();

  useEffect(() => {
    router.push(routes.home());
  }, [router]);

  return null;
};

export default NotFoundPage;
