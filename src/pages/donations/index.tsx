import React, { useEffect } from 'react';
import Head from 'next/head';
import { Alert, Typography } from '@mui/material';
import { fetchAllDonations } from '@/common/store/donations';
import MainLayout from '@/common/components/MainLayout';
import { useAppDispatch, useAppSelector } from '@/common/hooks/store';
import DonationsTable, {
  DonationsTableSkeleton,
} from '@/common/components/DonationsTable';

const DonationsPage = () => {
  const donations = useAppSelector((state) => state.donations.donations);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchAllDonations({}));
  }, [dispatch]);

  // function handlePageChange(
  //   event: React.MouseEvent<HTMLButtonElement> | null,
  //   newPage: number,
  // ) {
  //   dispatch(
  //     fetchAllDonations({
  //       limit: donations.data?.limit,
  //       offset: (donations.data?.limit || 0) * newPage,
  //     }),
  //   );
  // }

  // function handleRowsPerPageChange(
  //   event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  // ) {
  //   dispatch(
  //     fetchAllDonations({
  //       limit: parseInt(event.target.value),
  //       offset: 0,
  //     }),
  //   );
  // }

  function handleDisplayMore() {
    dispatch(
      fetchAllDonations({
        limit: donations.data?.limit,
        offset: (donations.data?.limit || 0) + (donations.data?.offset || 0),
      }),
    );
  }

  const renderData = () => {
    if (!!donations.data) {
      return (
        <DonationsTable
          count={donations.data.count}
          donations={donations.data.data}
          offset={donations.data.offset}
          onDisplayMore={handleDisplayMore}
          // onPageChange={handlePageChange}
          // onRowsPerPageChange={handleRowsPerPageChange}
          limit={donations.data.limit}
        />
      );
    }
    if (donations.pending) {
      return <DonationsTableSkeleton />;
    }
    if (donations.error) {
      return (
        <Alert severity="error" sx={{ marginTop: 4 }}>
          Une erreur est survenue.
        </Alert>
      );
    }
  };

  return (
    <>
      <Head>
        <title>Donations</title>
      </Head>
      <MainLayout>
        <Typography variant="h6" pb={2}>
          My payments
        </Typography>
        {renderData()}
      </MainLayout>
    </>
  );
};

export default DonationsPage;
