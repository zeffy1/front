import MainLayout from '@/common/components/MainLayout';
import { Typography } from '@mui/material';
import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <MainLayout>
        <Typography variant="h6">Home</Typography>
      </MainLayout>
    </>
  );
}
