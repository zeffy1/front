import { Roboto } from 'next/font/google';

const roboto = Roboto({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

const typography = {
  fontFamily: roboto.style.fontFamily,
  body1: {
    fontSize: 16,
  },
  body2: {
    fontSize: 14,
  },
};

export default typography;
