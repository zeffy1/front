import {
  PayloadAction,
  SerializedError,
  createAsyncThunk,
  createSlice,
} from '@reduxjs/toolkit';
import { apiGetDonations } from '../api/donations';

export const fetchAllDonations = createAsyncThunk(
  'donation/fetchAll',
  async ({ limit, offset }: { limit?: number; offset?: number }) => {
    const response = await apiGetDonations({ limit, offset });
    return response;
  },
);

export type DonationProps = {
  donation: {
    amount?: number;
    createdAtUtc: number;
    firstName: string;
    lastName: string;
  };
  id: string;
};

type DonationsState = {
  donations: {
    data: {
      count: number;
      data: Array<DonationProps>;
      offset: number;
      limit: number;
    } | null;
    error: SerializedError | null;
    pending: boolean;
  };
};

const initialState: DonationsState = {
  donations: { data: null, error: null, pending: false },
};

const donationsSlice = createSlice({
  name: 'donations',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllDonations.pending, (state) => {
      // state.donations.data = null;
      state.donations.error = null;
      state.donations.pending = true;
    });
    builder.addCase(
      fetchAllDonations.fulfilled,
      (
        state,
        action: PayloadAction<{
          count: number;
          data: Array<DonationProps>;
          limit: number;
          offset: number;
        }>,
      ) => {
        state.donations.data = {
          ...action.payload,
          data: [
            ...(state.donations.data ? state.donations.data.data : []),
            ...action.payload.data,
          ],
        };
        state.donations.pending = false;
      },
    );
    builder.addCase(fetchAllDonations.rejected, (state, action) => {
      state.donations.error = action.error;
      state.donations.pending = false;
    });
  },
});

export default donationsSlice.reducer;
