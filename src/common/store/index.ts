import { configureStore } from '@reduxjs/toolkit';

import donationsReducer from './donations';

export function makeStore() {
  return configureStore({
    reducer: {
      donations: donationsReducer,
    },
  });
}

const store = makeStore();

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
