import {
  Paper,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  // TablePagination,
  TableRow,
} from '@mui/material';
import { format } from 'date-fns';
import { useCallback, useEffect } from 'react';
import { DonationProps } from '../store/donations';

type DonationsTableProps = {
  count: number;
  donations: Array<DonationProps>;
  limit: number;
  offset: number;
  onDisplayMore: () => void;
  // onPageChange: (
  //   event: React.MouseEvent<HTMLButtonElement> | null,
  //   newPage: number,
  // ) => void;
  // onRowsPerPageChange: (
  //   event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  // ) => void;
};

const DonationsTable = ({
  count,
  donations,
  // limit,
  // offset,
  // onPageChange,
  // onRowsPerPageChange,
  onDisplayMore,
}: DonationsTableProps) => {
  const onScroll = useCallback(() => {
    if (
      window.innerHeight + Math.round(document.documentElement.scrollTop) >=
        document.body.offsetHeight &&
      count > donations.length
    ) {
      onDisplayMore();
    }
  }, [count, donations.length, onDisplayMore]);

  useEffect(() => {
    document.addEventListener('scroll', onScroll);
    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, [onScroll]);

  return (
    <Paper
      sx={{
        boxShadow: 'none',
        maxWidth: 1000,
        overflowX: 'auto',
        width: '100%',
      }}
    >
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Amount</TableCell>
            <TableCell>Contact</TableCell>
            <TableCell>Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {donations.map((donation, i) => {
            return (
              <TableRow key={`${donation.id}-${i}`}>
                <TableCell>${donation.donation.amount}</TableCell>
                <TableCell>
                  {donation.donation.firstName} {donation.donation.lastName}
                </TableCell>
                <TableCell>
                  {format(
                    new Date(donation.donation.createdAtUtc),
                    'd LLL K:mm a',
                  )}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
      {/* <TablePagination
        component="div"
        count={count}
        page={Math.floor(offset / limit)}
        onPageChange={onPageChange}
        rowsPerPage={limit}
        onRowsPerPageChange={onRowsPerPageChange}
      /> */}
    </Paper>
  );
};

export const DonationsTableSkeleton = ({ numberOfRows = 3 }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>Amount</TableCell>
        <TableCell>Contact</TableCell>
        <TableCell>Date</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {[...Array(numberOfRows)].map((_, i) => (
        <TableRow key={`row-${i}`}>
          {[...Array(3)].map((_, i) => (
            <TableCell key={`cell-${i}`}>
              <Skeleton />
            </TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  </Table>
);

export default DonationsTable;
