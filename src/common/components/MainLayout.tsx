import React, { ReactNode } from 'react';
import {
  Box,
  Container,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import { useRouter } from 'next/router';
import * as routes from '@/common/constants/routes';
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism';
import Image from 'next/image';

type MainLayoutProps = {
  children?: ReactNode;
};

const MainLayout = ({ children }: MainLayoutProps) => {
  const router = useRouter();

  function handleClickDonations() {
    router.push(routes.donations());
  }

  function handleClickZeffy() {
    router.push(routes.home());
  }

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
      }}
    >
      <Drawer anchor="left" open={true} variant="permanent">
        <Box sx={{ width: 250 }}>
          <Box
            sx={{
              cursor: 'pointer',
              display: 'flex',
              justifyContent: 'center',
              pb: 4,
              pt: 2,
            }}
            onClick={handleClickZeffy}
          >
            <Image alt="Zeffy" src="/zeffy.webp" height={43} width={140} />
          </Box>
          <List>
            <ListItem
              button
              onClick={handleClickDonations}
              sx={{
                bgcolor:
                  router.pathname === routes.donations() ? '#def7f3' : '',
              }}
            >
              <ListItemIcon sx={{ minWidth: 45 }}>
                <VolunteerActivismIcon />
              </ListItemIcon>
              <ListItemText primary="Payments" />
            </ListItem>
          </List>
        </Box>
      </Drawer>
      <Container
        component="main"
        maxWidth={false}
        sx={{
          alignItems: 'baseline',
          display: 'flex',
          flexDirection: 'column',
          maxWidth: 'none',
          pb: 4,
          pr: 4,
          pl: { xs: 36, ml: 36 },
          pt: 4,
        }}
      >
        {children}
      </Container>
    </Box>
  );
};

export default MainLayout;
