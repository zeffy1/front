import axios from 'axios';

export async function apiGetDonations({
  limit,
  offset,
}: {
  limit?: number;
  offset?: number;
}) {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_API_BASE_URL}/donations`,
    { params: { limit, offset } },
  );
  return response.data;
}
