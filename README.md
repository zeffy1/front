# Zeffy Front

[![license](https://img.shields.io/badge/license-Proprietary-blue.svg)](https://gitlab.com/zeffy1/front/-/blob/main/LICENSE)

  Website of the Zeffy application, powered by Next.js.

## Installation

```bash
$ nvm use
$ npm install
```

## Running the app

```bash
$ npm run dev
```

Open [http://localhost:3002](http://localhost:3002) with your browser to see the result.

## Authors

- [**Laetitia Dufau**](https://gitlab.com/laetidufau) &lt;[laeti.dufau@gmail.com](mailto:laeti.dufau@gmail.com)&gt; - Main developer

## License

This project is licensed under a proprietary license - see the [LICENSE](https://gitlab.com/zeffy1/front/-/blob/main/LICENSE) file for details.
